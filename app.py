from inference import Model

from flask import Flask, request, abort
import numpy as np
import requests
import json
import os
import re

app = Flask(__name__)

model_path = os.path.join("models", "vec", "michigan.vec")
ranker = Model(model_path)

def seperate_location_name(name_list):
    return [re.sub(r"(\w)([A-Z])", r"\1 \2", name) for name in name_list]

@app.route('/rank_names', methods = ['POST'])
def rank_names():
    try: #try to load post data
        message = json.loads(request.data)
    except json.JSONDecodeError:
        print(e)
        return abort(400)

    try:
        ranking_scores = ranker.rank_location_relevance(seperate_location_name(message["names"])).tolist()
    except Exception as e:
        print(f"An error occurred while trying to rank names: {e}") 
        return abort(500)

    if message.get("debug", 0):
        sorted_indices = np.argsort(ranking_scores)[::-1]
        names_with_ranking = [ {"Name" : message["names"][i] , "Score" : ranking_scores[i] } for i in sorted_indices ]
    else: 
        names_with_ranking = []

    return_message = {   
        "ranked" : ranking_scores, 
        "names" : names_with_ranking
    }

    return return_message

if __name__ == '__main__':
    app.run(host="0.0.0.0")