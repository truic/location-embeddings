#import fasttext
import os
import numpy as np

def load_word2vec_embeddings(path):
    embeddings_dict = {}
    with open(path, 'r', encoding='utf-8') as f:
        embeddings_num, embeddings_size = [int(dims) for dims in f.readline().split()]
        for line in f:
            line_values = line.split()
            embedding_index = len(line_values) - embeddings_size
            string_value = " ".join(line_values[:embedding_index])
            embedding = np.asarray([float(val) for val in line_values[embedding_index:]])
            embeddings_dict[string_value] = embedding

            embedding_index = 1

    return embeddings_dict

def load_glove_embeddings(path, embeddings_size):
    embeddings_dict = {}
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            line_values = line.split()
            embedding_index = len(line_values) - embeddings_size
            string_value = " ".join(line_values[:embedding_index])
            embedding = np.asarray([float(val) for val in line_values[embedding_index:]])
            embeddings_dict[string_value] = embedding

            embedding_index = 1

    return embeddings_dict

def cosine_similarity(arr_1, arr_2):
    return np.divide(np.dot(arr_1, arr_2), np.multiply(np.linalg.norm(arr_1), np.linalg.norm(arr_2)))

def get_most_similar(input_location, embeddings_dict, num = 10):
    main_embedding = embeddings_dict[input_location]

    names = np.asarray([name for name in embeddings_dict.keys()])
    embeddings = np.asarray([val for val in embeddings_dict.values()])

    similarities = [cosine_similarity(main_embedding, embb) for embb in embeddings]

    similarity_indices = np.argsort(similarities)[::-1]

    return [names[sim_index] for sim_index in similarity_indices[1:num + 1]]

def generate_ngrams(token_list, n_gram_size):
    n_grams = zip(*[token_list[i:] for i in range(n_gram_size)])
    return [" ".join(n_gram) for n_gram in n_grams] 

class Model:
    def __init__(self, path):
        self.model_path = path

        embeddings_dict = load_word2vec_embeddings(path)

        self.vocabulary_dict = {token: index + 1 for index, token in enumerate(embeddings_dict.keys())}
        
        embeddings_matrix = np.asarray([val for val in embeddings_dict.values()])

        self.embeddings_matrix = np.append(np.zeros((1, embeddings_matrix.shape[1])), embeddings_matrix, axis = 0)
    
    def get_token_embedding(self, token):
        return self.embeddings_matrix[self.vocabulary_dict.get(token, 0)]

    def get_token_embedding_by_index(self, index):
        return self.embeddings_matrix[index]    

    def create_name_n_grams(self, name):
        word_list = name.split()
        word_num = len(word_list)
        if word_num < 2:
            return [name]
        else:
            return [generate_ngrams(word_list, n_gram_size) for n_gram_size in range(word_num - 1, 0, -1)]

    def get_token_index_from_n_grams(self, n_grams_list):
        token_index = 0
        for dim_n_grams in n_grams_list:
            for n_gram in dim_n_grams:
                token_index = self.vocabulary_dict.get(n_gram, 0)
                if token_index:
                    return token_index
        return token_index

    def get_embeddings_from_n_grams(self, n_grams_list):
        token_index = 0
        for dim_n_grams in n_grams_list:
            for n_gram in dim_n_grams:
                token_index = self.vocabulary_dict.get(n_gram, 0)
                if token_index:
                    return self.get_token_embedding_by_index(token_index)
        return self.get_token_embedding_by_index(token_index)

    def rank_location_relevance(self, input_location, name_list):
        input_token_index = self.vocabulary_dict.get(input_location, 0)
        if not input_token_index:
            print(f"Input location {input_location} is not in vocabulary")
            return np.zeros((len(name_list)))
        else:
            input_embedding = self.get_token_embedding_by_index(input_token_index)
            name_list_location_tokens = [self.get_token_index_from_n_grams(self.create_name_n_grams(name)) for name in name_list]

            name_list_scores = [cosine_similarity(input_embedding, self.get_token_embedding_by_index(token_index)) if token_index else 0 for token_index in name_list_location_tokens]

            return name_list_scores

if __name__ == "__main__":

    model_path = "models/txt/opti_location_embeddings.txt"
    model_vec_path = "models/vec/michigan.vec"

    embeddings_dict = load_glove_embeddings(model_path, 100)
    #embeddings_dict = load_word2vec_embeddings(model_vec_path)


    for name in get_most_similar("Ann Arbor", embeddings_dict):
        print(name)





