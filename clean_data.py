import pandas as pd

raw_file_path = "data/raw/GeoNamesUS/US.txt"
clean_file_path = "data/clean/GeoNamesUS.tsv"
 
column_names = [
    "geonameid",         
    "name",              
    "asciiname",         
    "alternatenames",    
    "latitude",          
    "longitude",         
    "feature class",     
    "feature code",      
    "country code",      
    "cc2",               
    "admin1 code",       
    "admin2 code",       
    "admin3 code",      
    "admin4 code",       
    "population",        
    "elevation",         
    "dem",               
    "timezone",          
    "modification date", 
]
df = pd.read_csv(raw_file_path, sep='\t', header = None, names=column_names)
print(f"df length: {len(df)}")

feature_class_filter = ['A', 'P'] #for codes check the data/raw/GeoNamesUs/readme.txt
columns_to_keep = ["name", "asciiname", "alternatenames", "latitude", "longitude", "feature class", "feature code","admin1 code", "admin2 code", "admin3 code","population"]
clean_df = df[df["feature class"].isin(feature_class_filter)][columns_to_keep]
print(f"clean df length: {len(clean_df)}")

clean_df.to_csv(clean_file_path, sep = '\t', index=False)

