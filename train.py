#%%
import networkx as nx
from node2vec import Node2Vec
from node2vec.edges import HadamardEmbedder

#%%
graph_file_path = 'data/graph/Michigan.edgelist'
embeddings_file_path = 'models/vec/michigan.vec'
model_file_path = 'models/bin/michigan'
graph = nx.read_edgelist(graph_file_path, delimiter = '\t')
# %%
node2vec = Node2Vec(graph, dimensions=128, walk_length=30, num_walks=200, workers=64)
model = node2vec.fit(window=10, min_count=1, batch_words=8)
# %%

print(model.wv.most_similar("Ann Arbor"))
# %%
model.wv.save_word2vec_format(embeddings_file_path)
# %%
model.save(model_file_path)

# edges_embs = HadamardEmbedder(keyed_vectors=model.wv)
# edges_kv = edges_embs.as_keyed_vectors()
# edges_kv.save_word2vec_format(embeddings_file_path)
# %%
