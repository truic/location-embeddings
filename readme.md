Download the US dataset from geonames

https://download.geonames.org/export/dump/US.zip


Linux: 

```
curl https://download.geonames.org/export/dump/US.zip -o data/raw/GeoNames/US.zip
```

```
unzip data/raw/US.zip -d data/raw/
```