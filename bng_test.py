from inference import Model

import math
import os
import numpy as np
import re
import json
import time

with open('data/input_examples/response_1618423357991.json') as f:
  data = json.load(f)

name_list = data['gen'] 

def seperate_location_name(name_list):
    return [re.sub(r"(\w)([A-Z])", r"\1 \2", name) for name in name_list]
    

if __name__ == "__main__":
    model_vec_path = "models/vec/michigan.vec"

    input_location = "Ann Arbor"

    names_to_take = len(name_list)

    model = Model(model_vec_path)

    new_name_list = seperate_location_name(name_list)

    print(new_name_list[:names_to_take])
    #print(model.create_name_n_grams(new_name_list[:2]))
    start = time.time()
    name_scores = model.rank_location_relevance(input_location, new_name_list[:names_to_take])
    end = time.time()
    score_indices = np.argsort(name_scores)[::-1]

    sorted_names = [new_name_list[sim_index] for sim_index in score_indices]

    for name in sorted_names:
        print(name)

    print(f"ranking perf in sec: {end - start}")

    #embeddings_dict = load_glove_embeddings(model_path, 100)
    #embeddings_dict = load_word2vec_embeddings(model_vec_path)


    # for name in get_most_similar("Ann Arbor", embeddings_dict):
    #     print(name)
