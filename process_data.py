# %% 
import os
import pandas as pd
import networkx as nx
import geopy.distance
from math import sin, cos, sqrt, atan2, radians


graph_name = "MichiganPlusPlus"

clean_file_path = "data/clean/GeoNamesUS.tsv"
graph_file_path = os.path.join('data/graph/', graph_name + ".edgelist")

#[admin1 code] is state

# admin2 code is county
# feature code: ADM2 is county

# %%
df = pd.read_csv(clean_file_path, sep='\t')

#%%
michigan_df = df[df['admin1 code'] == 'MI']#.drop('admin1 code', axis = 1)
print(len(michigan_df))
#%%
michigan_df.loc[michigan_df['feature code'] == "ADM2"]
michigan_df.loc[michigan_df['name'] == 'Beaver']
michigan_df.loc[michigan_df['name'] == 'Ann Arbor']
#%%

#%%
# ppl_df = michigan_df.loc[michigan_df['feature code'] == 'PPL']
# ppl_df.loc[ppl_df['name'] == 'Ann Arbor']
michigan_df.loc[michigan_df['name'] == 'Michigan']
# %%
AnnArborRow = michigan_df.loc[michigan_df['name'] == 'Ann Arbor']
DetroitRow = michigan_df.loc[michigan_df['name'] == 'Detroit']

coordinate1 = AnnArborRow[["latitude", "longitude"]].values[0]
coordinate2 = DetroitRow[["latitude", "longitude"]].values[0]
geopy.distance.distance(coordinate1, coordinate2)

# %% 
def build_state_graph(graph, state_name, state_df):
    county_groups = state_df.groupby(["admin2 code"]) # group entities by county 
    for county_code, county_df in county_groups: # add connection between county and the entities in it
        name_of_county = county_df.loc[county_df['feature code'] == "ADM2"]["name"].values[0].strip()
        graph.add_edge(state_name, name_of_county, weight=1.0)
        locations_in_county = county_df["name"].tolist() #.loc[county_df['population'] >= 2500] #only get locations with a population of 2500 or larger
        graph.add_edges_from([(name_of_county, name.strip(), {"weight" : 1.0}) for name in locations_in_county if name.strip() != name_of_county])
    
    return graph
    
#%%
#maybe have distance also be dependent on population of nearby cities?
def build_city_distance_graph(graph, df, max_distance):
    locations_to_connect_df = df[df['feature code'].isin(["PPL", "PPLA", "PPLA2"])]
    for index, row in locations_to_connect_df.iterrows():
        center_location_name = row["name"].strip()
        center_location_coordinates = row[["latitude", "longitude"]].to_numpy()

        other_locations = locations_to_connect_df[~locations_to_connect_df.index.isin([index])]

        other_locations["distance"] = [geopy.distance.distance(center_location_coordinates, coord) for coord in other_locations.loc[["latitude", "longitude"]].to_numpy()]

        nearby_locations = other_locations[other_locations["distance"] <= max_distance]

        # for name_dist in nearby_locations[["name", "distance"]]:
        #     return name_dist["name"]

        return nearby_locations["distance"]
    



# %% 
graph = nx.Graph()

michigan_df = df[df['admin1 code'] == 'MI']#.drop('admin1 code', axis = 1)
name_of_state = michigan_df.loc[michigan_df['feature code'] == "ADM1"]["name"].values[0].strip()

build_state_graph(graph, name_of_state, michigan_df)
print(f"Number of nodes: {len(graph)}")

#%%
build_city_distance_graph(nx.Graph(), michigan_df, 65)
# %%
nx.write_edgelist(graph, graph_file_path, delimiter = '\t')
# %%
